import './App.css';
import Links from "./Links/Links";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Numbers from "./Lottery/Numbers";
import CountryInfo from "./CountryInfo/CountryInfo";
import ModalView from "./ModalView/ModalView";

const App = () => {
  return (
      <div className="App">
        <h1>Мои работы</h1>
          <BrowserRouter>
              <Links/>
              <Switch>
                  <Route path = '/lottery' component={Numbers}/>
                  <Route path = '/country' component={CountryInfo}/>
                  <Route path = '/modal' component={ModalView}/>

              </Switch>
          </BrowserRouter>

      </div>
  );
};

export default App;
