import React from 'react';
import {NavLink} from "react-router-dom";
import './Links.css';

const Links = () => {
    return (
        <div className='link'>
            <NavLink exact to='/lottery'>
               Проект "Лоторея"
            </NavLink>
            <NavLink exact to='/country'>
                Проект "Границы страны"
            </NavLink>
            <NavLink exact to='/modal'>
              Проект "Всплывающее окно"
            </NavLink>
        </div>
    );
};

export default Links;