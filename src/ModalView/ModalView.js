import {useState} from "react";
import Modal from "../Modal/Modal";
import './Modal.css';

const ModalView = () => {

    const [show, setShow] = useState(false);

    const modalHandler = () => {
        setShow(true);
    };

    const cancelHandler = () => {
        setShow(false);
    };

    return (
        <div className="Modalview">
            <Modal
                show = {show}
                title = 'Здесь будет название окна'
                cancel = {cancelHandler}
            />
            <button className='modal-btn' onClick={modalHandler}>Показать</button>
        </div>
    );
};

export default ModalView;